package garden_planner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * A simple garden planner tool for costing various layouts of garden beds.
 *
 * @author Mark Utting
 */
public class GardenPlanner {
	public final String GARDEN_PLANNER_VERSION = "Garden Planner v0.1";
	private final double SOIL_DEPTH = 0.2;  // metres
	private final double soilPrice;
	private final double wallPrice;
	
	/** The collection of garden beds in the current design. */
	private ArrayList<GeometricObject> beds = new ArrayList<>();

	/**
	 * Creates a new garden planner, with the given soil and wall prices.
	 * 
	 * @param soilPerCubicMetre Price of garden soil, per cubic metre.
	 * @param wallPerMetre Price of the wall of a garden bed, per metre.
	 */
	public GardenPlanner(double soilPerCubicMetre, double wallPerMetre) {
		soilPrice = soilPerCubicMetre;
		wallPrice = wallPerMetre;
	}

	/** Adds one shape to the garden layout. */
	public void addBed(GeometricObject bed) {
		beds.add(bed);
	}

	/** Reads a text file and adds one shape/line to the garden layout. */
	public void parseBeds(Scanner in) {
		while (in.hasNext()) {
			String line = in.nextLine().trim();
			String[] words = line.split(" +");
			// System.out.println(Arrays.toString(words));  // just for debugging
			if (line.startsWith("#") || line.length() == 0) {
				// we skip comment lines and empty lines.
			} else if (words.length == 3 && words[0].toLowerCase().equals("rectangle")) {
				addBed(new GeometricObject(Double.parseDouble(words[1]), Double.parseDouble(words[2])));
			} else {
				System.out.println("ERROR: illegal garden bed: " + line);
			}
		}
	}

	/** Calculates and prints total costs of the current garden layout. */
	public void calculatePrices() {
		System.out.println(GARDEN_PLANNER_VERSION);
		double wallLength = 0.0;
		double gardenArea = 0.0;
		for (GeometricObject bed : beds) {
			System.out.println("  added " + bed);
			gardenArea += bed.getArea();
			wallLength += bed.getPerimeter();
		}
		final double wallCost = wallLength * wallPrice;
		final double soilVolume = gardenArea * SOIL_DEPTH;
		final double soilCost = soilVolume * soilPrice;
		System.out.printf("Total wall length is %.2f m, cost $%.2f.\n", wallLength, wallCost);
		System.out.printf("Total garden area is %.2f m2 (%.2f m3 of soil), cost $%.2f.\n", gardenArea, soilVolume, soilCost);
		System.out.printf("Total cost is: $%.2f\n", (wallCost + soilCost));
	}

	/**
	 * Calculates costs for a given garden layout.
	 * This can take up to three optional command line arguments.
	 *
	 * @param args [soilPricePerCubicMetre wallPricePerMetre  [designFile.txt]]
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		// Example prices are roughly based on http://centenarylandscaping.com.au.
		double gardenSoil = 90.00 * 0.9;   // 1 cubic metre of Ultima Organic Garden Soil.  
		double sleeperCost = 51.00 / 3.0;  // 200x75mm CCA Hardwood Sleeper, 3.0m long.
		if (args.length >= 2) {
			gardenSoil = Double.parseDouble(args[0]);
			sleeperCost = Double.parseDouble(args[1]);
		}
		GardenPlanner planner = new GardenPlanner(gardenSoil, sleeperCost);
		if (args.length == 3) {
			planner.parseBeds(new Scanner(new File(args[2])));
		} else {
			// use our default layout: two rectangles with a square in the middle
			planner.addBed(new GeometricObject(2.0, 1.0));
			planner.addBed(new GeometricObject(2.0, 2.0));
			planner.addBed(new GeometricObject(2.0, 1.0));
		}
		planner.calculatePrices();
	}

}
