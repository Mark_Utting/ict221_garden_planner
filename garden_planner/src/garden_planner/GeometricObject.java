package garden_planner;

/**
 * This class represents a 2D geometric shape.
 *
 * Currently, it is limited to simple rectangular shapes.
 *
 * @author Mark Utting
 */
public class GeometricObject {

	private final double width;
	private final double height;
	
	public GeometricObject(double w, double h) {
		width = w;
		height = h;
	}

	/** @return the total internal area of the shape. */
	public double getArea() {
		return width * height;
	}

	/** @return the total length of the edges of the shape. */ 
	public double getPerimeter() {
		return 2 * (width + height);
	}
	
	@Override
	public String toString() {
		return "Rectangle " + width + " " + height;
	}

}
